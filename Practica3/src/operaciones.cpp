#include <chrono>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <random>
#include <vector>

#define GREEDY    0
#define DINAMICA  1

#define SUMA '+'
#define RESTA '-'
#define MULTIPLICACION '*'
#define DIVISION '/'
#define NUM_OPERACIONES 4

using namespace std;
using namespace std::chrono;

const char tiposOperacion[NUM_OPERACIONES] = {SUMA, RESTA, MULTIPLICACION, DIVISION};

struct operacion{
  int operando;
  char operador;
};

int computar(int num, operacion op){
  int sol = num;
  switch(op.operador){
    case SUMA:
    {
      sol += op.operando;
    }
    break;
    case RESTA:
    {
      sol -= op.operando;
    }
    break;
    case MULTIPLICACION:
    {
      sol *= op.operando;
    }
    break;
    case DIVISION:
    {
      sol /= op.operando;
    }
    break;
  }
  return sol;
}

int computar (vector<operacion> v){
  int sol = 0;
  for(int i=0; i<v.size(); i++){
    sol = computar(sol, v[i]);
  }
  return sol;
}

int computar (vector<operacion> v, operacion op){
  int sol = computar(v);
  sol = computar(sol, op);
  return sol;
}

int coste(operacion op, vector<operacion> solucion, int objetivo){
  int sol = computar(solucion, op);

  int coste = abs(objetivo - sol);

  return coste;
}

int seleccionar(vector<int> candidatos, vector<operacion> solucion, int objetivo, char& operador){
  if(candidatos.size() == 0){
    return -1;
  }
  else{
    int i_elegido = 0;
    operacion c;
    c.operando = candidatos[i_elegido];
    c.operador = operador = tiposOperacion[0];
    int coste_min = coste(c, solucion, objetivo);
    int coste_actual;
    for(int i=0; i<candidatos.size(); i++){
      c.operando = candidatos[i];
      for(int j=0; j<NUM_OPERACIONES; j++){
        c.operador = tiposOperacion[j];

        if(!(c.operador == DIVISION &&
            (c.operando == 0 || computar(solucion) % c.operando != 0))){
              coste_actual = coste(c, solucion, objetivo);
              if(coste_actual < coste_min){
                i_elegido = i;
                operador = tiposOperacion[j];
                coste_min = coste_actual;
              }
        }
      }
    }
    return i_elegido;
  }
}

bool esSolucion(vector<operacion> parcial, int objetivo){
  return( computar(parcial) == objetivo);
}


void operacionesGreedy(vector<operacion>& solucion, vector<int>& candidatos, int objetivo){
  if(!esSolucion(solucion, objetivo)){
    operacion candidato;
    int i_sel = seleccionar(candidatos, solucion, objetivo, candidato.operador);
    if(i_sel > 0){
      candidato.operando = candidatos[i_sel];
      solucion.push_back(candidato);
      candidatos.erase(candidatos.begin()+i_sel);
      operacionesGreedy(solucion, candidatos, objetivo);
    }
  }
}

vector<operacion> operaciones(vector<int> candidatos, int objetivo){
  vector<operacion> sol;
  if(candidatos.size() > 0){
    operacionesGreedy(sol,candidatos, objetivo);
  }
  return sol;
}

void sintaxis(){
  cerr << "Sintaxis: OPERACIONES <tam> <algoritmo>" << endl;
  cerr << "Tamaño debe ser mayor que 0." << endl;
  cerr << "Algoritmos:" << endl;
  cerr << "  - greedy" << endl;
  cerr << "  - dinamica" << endl;
}

int stringToAlg(string alg){
  if(strcmp(alg.c_str(), "greedy") == 0){
    return GREEDY;
  }
  else if(strcmp(alg.c_str(), "dinamica") == 0){
    return DINAMICA;
  }
  else{
    return -1;
  }
}

vector<int> generaVector(int tam){
  vector<int> v;
  for(int i=0; i<tam; i++){
    v.push_back(rand() % (tam));
  }
  return v;
}

int main(int argc, char* argv[]){

  // Lectura y comprobación de parámetros
  if(argc != 3){
    sintaxis();
    exit(0);
  }

  int tam = atoi(argv[1]);
  string alg_str = argv[2];
  int algoritmo = stringToAlg(alg_str);

  if(algoritmo == -1 || tam < 0){
    cerr << "ERROR: Parámetros incorrectos." << endl;
    sintaxis();
    exit(-1);
  }

  // Generación de datos
  srand(time(0));
  vector<int> candidatos = generaVector(tam);
  int objetivo = rand() % (tam*2);

  cout << "Objetivo: " << objetivo << endl;
  cout << "Elementos del conjunto:" << endl;
  for(int i=0; i<tam; i++)
    cout << candidatos[i] << " ";
  cout << endl;

  // Medición de tiempo
  high_resolution_clock::time_point start, end;
  duration<double> tiempo_transcurrido;
  start = high_resolution_clock::now();

  vector<operacion> solucion = operaciones(candidatos, objetivo);

  // Generación de resultados
  end = high_resolution_clock::now();
  tiempo_transcurrido  = duration_cast<duration<double> >(end - start);
  cout << tam << "\t" <<tiempo_transcurrido.count() << endl;

  if(solucion.size() > 0){
    cout << "Solución:" << endl;
    for(int i=0; i<solucion.size(); i++){
      cout << solucion[i].operador << solucion[i].operando;
    }
    cout << " = " << computar(solucion) << endl;
  }
  else{
    cout << "No existe solución al problema" << endl;
  }
}

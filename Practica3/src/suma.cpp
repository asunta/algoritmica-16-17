#include <chrono>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <random>
#include <vector>

#define GREEDY    0
#define DINAMICA  1

using namespace std;
using namespace std::chrono;

int sumatoria (vector<int> v){
  int sum = 0;
  for(int i=0; i<v.size(); i++){
    sum += v[i];
  }
  return sum;
}

int coste(int candidato, vector<int> solucion, int objetivo){
  int suma = sumatoria(solucion) + candidato;

  int coste = objetivo - suma;

  if(coste < 0){
    coste = objetivo+1;
  }

  return coste;
}

int seleccionar(vector<int> candidatos, vector<int> solucion, int objetivo){
  if(candidatos.size() == 0){
    return -1;
  }
  else{
    int i_elegido = 0;
    int coste_min = coste(candidatos[i_elegido], solucion, objetivo);
    int coste_actual;
    for(int i=1; i<candidatos.size(); i++){
      coste_actual = coste(candidatos[i], solucion, objetivo);
      if(coste_actual < coste_min){
        i_elegido = i;
        coste_min = coste_actual;
      }
    }
    return i_elegido;
  }
}



bool factible(vector<int> solucion, int seleccionado, int objetivo){
  int suma = sumatoria(solucion) + seleccionado;
  return (suma <= objetivo);
}

bool esSolucion(vector<int> parcial, int objetivo){
  return( sumatoria(parcial) == objetivo);
}

void sumaGreedy(vector<int>& solucion, vector<int>& candidatos, int objetivo){
  if(!esSolucion(solucion, objetivo)){
    int i_sel = seleccionar(candidatos, solucion, objetivo);
    if(i_sel >= 0 && factible(solucion, candidatos[i_sel], objetivo)){
      solucion.push_back(candidatos[i_sel]);
      candidatos.erase(candidatos.begin()+i_sel);
      sumaGreedy(solucion, candidatos, objetivo);
    }
  }
}

vector<vector<int>> construirTabla(vector<int> candidatos, int objetivo){
  vector<vector<int>> tabla;
  // Llenamos la tabla de ceros, reservando a su vez memoria
  for(int i=0; i<= candidatos.size(); i++){
    vector<int> fila;
    for(int j=0; j<=objetivo; j++){
      fila.push_back(0);
    }
    tabla.push_back(fila);
  }

  // Algoritmo PD
  for(int i=1; i<= candidatos.size(); i++){
    for(int w=1; w <= objetivo; w++){
      if(candidatos[i-1] <= w &&
        tabla[i-1][w] < candidatos[i-1] + tabla[i-1][w - candidatos[i-1]]){
          tabla[i][w] = candidatos[i-1] + tabla[i-1][w - candidatos[i-1]];
      }
      else{
        tabla[i][w] = tabla[i-1][w];
      }
    }
  }

  return tabla;
}

vector<int> extraerSolucion(vector<vector<int>> tabla, vector<int> candidatos){
  vector<int> sol;
  int k = tabla[0].size() - 1;
  for(int i=candidatos.size(); i>0; i--){
    if(tabla[i][k] != tabla[i-1][k]){
      sol.push_back(candidatos[i-1]);
      k = k - candidatos[i-1];
    }
  }
  return sol;
}

void sumaDinamica(vector<int>& solucion, vector<int> candidatos, int objetivo){
  vector<vector<int>> tabla = construirTabla(candidatos, objetivo);
  solucion = extraerSolucion(tabla, candidatos);
}

vector<int> suma(vector<int> candidatos, int objetivo, int algoritmo){
  vector<int> suma;
  if(candidatos.size() > 0){
    switch(algoritmo){
      case GREEDY:
      {
        sumaGreedy(suma,candidatos, objetivo);
      }
      break;
      case DINAMICA:
      {
        sumaDinamica(suma, candidatos, objetivo);
      }
      break;
    }

  }
  return suma;
}

void sintaxis(){
  cerr << "Sintaxis: SUMA <tam> <algoritmo>" << endl;
  cerr << "Tamaño debe ser mayor que 0." << endl;
  cerr << "Algoritmos:" << endl;
  cerr << "  - greedy" << endl;
  cerr << "  - dinamica" << endl;
}

int stringToAlg(string alg){
  if(strcmp(alg.c_str(), "greedy") == 0){
    return GREEDY;
  }
  else if(strcmp(alg.c_str(), "dinamica") == 0){
    return DINAMICA;
  }
  else{
    return -1;
  }
}

vector<int> generaVector(int tam){
  vector<int> v;
  for(int i=0; i<tam; i++){
    v.push_back(rand() % (tam));
  }
  return v;
}

int main(int argc, char* argv[]){

  // Lectura y comprobación de parámetros
  if(argc != 3){
    sintaxis();
    exit(0);
  }

  int tam = atoi(argv[1]);
  string alg_str = argv[2];
  int algoritmo = stringToAlg(alg_str);

  if(algoritmo == -1 || tam < 0){
    cerr << "ERROR: Parámetros incorrectos." << endl;
    sintaxis();
    exit(-1);
  }

  // Generación de datos
  srand(time(0));
  vector<int> candidatos = generaVector(tam);
  int objetivo = rand() % (tam*2) + 1;

  // vector<int> candidatos = {5, 5, 0, 7, 3, 7, 3, 3, 8, 6};
  // int objetivo = 14;

  cout << "Objetivo: " << objetivo << endl;
  cout << "Elementos del conjunto:" << endl;
  for(int i=0; i<tam; i++)
    cout << candidatos[i] << " ";
  cout << endl;

  // Solución del problema

  // Medición de tiempo
  high_resolution_clock::time_point start, end;
  duration<double> tiempo_transcurrido;
  start = high_resolution_clock::now();

  vector<int> solucion = suma(candidatos, objetivo, algoritmo);

  // Generación de resultados
  end = high_resolution_clock::now();
  tiempo_transcurrido  = duration_cast<duration<double> >(end - start);
  cout << tam << "\t" <<tiempo_transcurrido.count() << endl;

  if(solucion.size() > 0){
    cout << "Solución:" << endl;
    cout << solucion[0];
    int suma = solucion[0];
    for(int i=1; i<solucion.size(); i++){
      cout << " + " << solucion[i];
      suma += solucion[i];
    }
    cout << " = " << suma << endl;
  }
  else{
    cout << "No existe solución al problema" << endl;
  }
}

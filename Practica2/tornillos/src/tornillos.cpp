#include <iostream>
#include <vector>
#include <utility>
#include <chrono>
#include <cstdlib>
#include <algorithm>

using namespace std;
using namespace std::chrono;

void generar(vector<unsigned int>& tornillos, vector<unsigned int>& tuercas, int tam){
  vector<int> buffer;

  for(int i=0; i<tam; i++){
    buffer.push_back(i);
  }

  random_shuffle(buffer.begin(), buffer.end());

  for(int i=0; i<tam; i++){
    tornillos.push_back(buffer[i]);
  }

  random_shuffle(buffer.begin(), buffer.end());

  for(int i=0; i<tam; i++){
    tuercas.push_back(buffer[i]);
  }
  // Permutacion per(tam, 1);
  // tornillos = *(per.begin());
  //
  // srand(time(0));
  //
  // int max = 100000;
  // if(per.NumeroPermutacionesPosibles() < max)
  //   max = per.NumeroPermutacionesPosibles();
  //
  // int next_per = rand() % max;
  //
  // for(int i=0; i<next_per; i++){
  //   per.GeneraSiguiente();
  // }
  // tuercas = *(per.begin());
}

void swap(unsigned int* xp, unsigned int* yp){
  unsigned int aux = *xp;
  *xp = *yp;
  *yp = aux;
}

void ordenar(vector<unsigned int>& tornillos, vector<unsigned int>& tuercas){
  // CASO BASE
  if(tornillos.size() == 2){
    if(tornillos[0] != tuercas[0]){
      swap(&(tornillos[0]), &(tornillos[1]));
    }
  }

  // CASO GENERAL
  else if(tornillos.size() > 2){
    int pivote = tuercas[0];
    int pos_pivote_tor;

    // Clasificamos los tornillos
    vector<unsigned int> tor_menores;
    vector<unsigned int> tor_mayores;

    for(int i=0; i<tornillos.size(); i++){
      if(tornillos[i] < tuercas[0])
        tor_menores.push_back(tornillos[i]);
      else if(tornillos[i] > tuercas[0])
        tor_mayores.push_back(tornillos[i]);
      else
        pos_pivote_tor = i;
    }



    // Clasificamos las tuercas
    vector<unsigned int> tue_menores;
    vector<unsigned int> tue_mayores;

    for(int i=0; i<tuercas.size(); i++){
      if(tuercas[i] < tornillos[pos_pivote_tor])
        tue_menores.push_back(tuercas[i]);
      else if(tuercas[i] > tornillos[pos_pivote_tor])
        tue_mayores.push_back(tuercas[i]);
    }

    // Recursión
    ordenar(tor_mayores, tue_mayores);
    ordenar(tor_menores, tue_menores);

    // Unión de resultados

    tornillos.clear();
    tuercas.clear();

    tornillos.push_back(pivote);
    tuercas.push_back(pivote);

    for(int i=0; i<tor_menores.size(); i++){
      tornillos.push_back(tor_menores[i]);
      tuercas.push_back(tue_menores[i]);
    }

    for(int i=0; i<tor_mayores.size(); i++){
      tornillos.push_back(tor_mayores[i]);
      tuercas.push_back(tue_mayores[i]);
    }
  }
}

int main(int argc, char* argv[]){
  if(argc != 2){
    cerr << "Sintaxis: " << argv[0] << " <tam>" << endl;
    exit(0);
  }

  int tam = atoi(argv[1]);

  if(tam <= 0){
    cerr << "El tamaño debe ser mayor que 0" << endl;
    exit(-1);
  }
  vector <unsigned int> tornillos, tuercas;
  generar(tornillos, tuercas, tam);

  cout << "Tornillos:" << endl;
  for(int i=0; i<tam; i++){
    cout << tornillos[i] << " ";
  }
  cout << endl;

  cout << "Tuercas:" << endl;
  for(int i=0; i<tam; i++){
    cout << tuercas[i] << " ";
  }
  cout << endl;

  ordenar(tornillos, tuercas);

  cout << "Tornillos ordenados:" << endl;
  for(int i=0; i<tam; i++){
    cout << tornillos[i] << " ";
  }
  cout << endl;

  cout << "Tuercas ordenadas:" << endl;
  for(int i=0; i<tam; i++){
    cout << tuercas[i] << " ";
  }
  cout << endl;
}

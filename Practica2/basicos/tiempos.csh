#!/bin/csh
if ($#argv != 2) then
  echo "Sintaxis: $0 <caso> <algoritmo>"
  echo "Casos posibles (número entero)"
  echo "  0. Caso mejor"
  echo "  1. Caso peor"
  echo "  2. Caso promedio"
  echo "Algoritmos:"
  echo "  - maxmin"
  echo "  - moda"
else
  set algoritmo = "$2"
  set fichero = "datos/tiempos_${algoritmo}"

  @ inicio = 100
  @ fin = 20000
  @ incremento = 100

  @ i = $inicio
  @ caso = $1


  if ($caso == 0) then
    set fichero = "${fichero}_mejor.dat"
  else if ($caso == 1) then
    set fichero = "${fichero}_peor.dat"
  else
    set fichero = "${fichero}_promedio.dat"
  endif

  echo > $fichero
  while ( $i <= $fin )
    echo Ejecución tam = $i
    echo `./bin/basicos $i $algoritmo $caso` >> $fichero
    @ i += $incremento
  end
endif

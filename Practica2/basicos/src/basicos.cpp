#include <iostream>
#include <vector>
#include <utility>
#include <chrono>
#include <cstdlib>
#include <cstring>

#define CASO_MEJOR 0
#define CASO_PEOR 1
#define CASO_PROMEDIO 2

using namespace std;
using namespace std::chrono;

int recursionModa(vector<int>& het, vector<int>& hom){
  // Seleccion de pivote
  int pivote = het[het.size() / 2];

  // División en subproblemas
  vector<int> iguales;
  vector<int> menores;
  vector<int> mayores;

  for(int i=0; i<het.size(); i++){
    if(het[i] == pivote)
      iguales.push_back(het[i]);
    else if(het[i] < pivote)
      menores.push_back(het[i]);
    else
      mayores.push_back(het[i]);
  }

  if(iguales.size() > hom.size()){
    hom = iguales;
  }

  // Búsqueda del óptimo
  if(hom.size() > menores.size() &&
    hom.size() > mayores.size()){
      return hom[0];
  }

  // Actualización de heterogéneos
  het.clear();

  for(int i=0; i<menores.size(); i++)
    het.push_back(menores[i]);
  for(int i=0; i<mayores.size(); i++)
    het.push_back(mayores[i]);

  // Recursión
  return recursionModa(het, hom);
}

int Moda(const vector<int>& v){
  // CASO BASE
  if(v.size() == 1){
    return v[0];
  }

  // CASO GENERAL
  else{
    vector<int> homogeneos;
    vector<int> heterogeneos(v);

    return recursionModa(heterogeneos, homogeneos);
  }
}

pair<int ,int> Max_Min(const vector<int> & v){
  pair<int, int> maxmin;

  // CASO BASE
  if(v.size() == 1){
    maxmin.first = v[0];
    maxmin.second = v[0];
  }
  else if(v.size() == 2){
    if(v[0] > v[1]){
      maxmin.first = v[0];
      maxmin.second = v[1];
    }
    else{
      maxmin.first = v[1];
      maxmin.second = v[0];
    }
  }

  // CASO GENERAL
  else{
    // División en subproblemas
    vector<int> izq, der;
    for(int i=0; i<v.size()/2; i++){
      izq.push_back(v[i]);
    }
    for(int i=v.size()/2; i<v.size(); i++){
      der.push_back(v[i]);
    }

    // Recursión
    pair<int,int> maxmin_izq = Max_Min(izq);
    pair<int,int> maxmin_der = Max_Min(der);

    // Unión
    if(maxmin_izq.first > maxmin_der.first){
      maxmin.first = maxmin_izq.first;
    }
    else{
      maxmin.first = maxmin_der.first;
    }

    if(maxmin_izq.second < maxmin_der.second){
      maxmin.second = maxmin_izq.second;
    }
    else{
      maxmin.second = maxmin_der.second;
    }
  }

  return maxmin;
}

vector<int> vectorAleatorio(int tam){
  vector<int> generado;
  srand(time(0));
  for (int i=0; i<tam; i++)
    generado.push_back(rand() % tam);
  return generado;
}

vector<int> vectorRepetidos(int tam){
  vector<int> generado;
  for(int i=0; i<tam; i++){
    generado.push_back(tam);
  }
  return generado;
}

vector<int> vectorSinRepetidos(int tam){
  vector<int> generado;
  for(int i=0; i<tam; i++){
    generado.push_back(i);
  }
  return generado;
}

vector<int> generaVector(int tam, string algoritmo, int caso){
  if(strcmp(algoritmo.c_str(), "maxmin") == 0){
    return vectorAleatorio(tam);
  }
  else if(strcmp(algoritmo.c_str(), "moda") == 0){
    switch(caso){
      case  CASO_PEOR:
      {
        return vectorSinRepetidos(tam);
      }
      break;
      case CASO_MEJOR:
      {
        return vectorRepetidos(tam);
      }
      break;
      case CASO_PROMEDIO:
      {
        return vectorAleatorio(tam);
      }
      break;
      default:
      {
        cerr << "El caso especificado no es un caso válido" << endl;
        vector<int> vacio;
        return vacio;
      }
      break;
    }
  }
  else{
    cerr << "El algoritmo especificado no existe" << endl;
    vector<int> vacio;
    return vacio;
  }
}

void sintaxis(){
  cerr << "Sintaxis de los argumentos:" << endl;
  cerr << "  BASICOS <tamaño> <algoritmo> <caso>" << endl;
  cerr << "  Algoritmo:" << endl;
  cerr << "  - maxmin" << endl;
  cerr << "  - moda" << endl;
  cerr << "  Caso:" << endl;
  cerr << "  0. Caso peor" << endl;
  cerr << "  1. Caso mejor" << endl;
  cerr << "  2. Caso promedio" << endl;
}

int main(int argc, char* argv[]){

  if(argc != 4){
    sintaxis();
    exit(0);
  }

  int tam = atoi(argv[1]);
  string algoritmo = argv[2];
  int caso = atoi(argv[3]);

  if(tam<0 || caso<0 || caso>2){
    sintaxis();
    exit(0);
  }

  vector<int> v = generaVector(tam, algoritmo, caso);
  cout << "Vector generado:" << endl;
  for(int i=0; i<tam; i++){
    cout << v[i] << " ";
  }
  cout << endl;

  if(v.size() > 0){
    // Medición de tiempo
    high_resolution_clock::time_point start, end;
    duration<double> tiempo_transcurrido;
    start = high_resolution_clock::now();

    if(strcmp(algoritmo.c_str(), "maxmin") == 0){
      pair<int, int> maxmin = Max_Min(v);
      cout << "Máximo: " << maxmin.first << endl;
      cout << "Mínimo: " << maxmin.second << endl;
    }
    else{
      int moda = Moda(v);
      cout << "Moda: " << moda << endl;
    }

    // Generación de resultados
    end = high_resolution_clock::now();
    tiempo_transcurrido  = duration_cast<duration<double> >(end - start);
    cout << tam << "\t" <<tiempo_transcurrido.count() << endl;
  }
}

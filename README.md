# Algoritmica

Prácticas de la asignatura de Algorítmica en la UGR, curso 2016-17.

![](https://img.shields.io/badge/subject-Algorithms-orange.svg)
[![](https://img.shields.io/badge/license-GNU-blue.svg)](http://www.gnu.org/copyleft/gpl.html)
[![](https://img.shields.io/badge/university-Granada-orange.svg)](http://www.ugr.es/)

## Contenido

- **Practica 1** : Estudio teórico y empírico de eficiencia de algoritmos.

#include <iostream>
#include <cstdlib>  // Para generación de números pseudoaleatorios
#include <chrono>// Recursos para medir tiempos
#include <string>
#include "algoritmos.h"

using namespace std;
using namespace std::chrono;

/**
* @brief Mensaje de error que informa de la sintaxis de los parámetros
*        de entrada
**/
void sintaxis()
{
  cerr << "Sintaxis:" << endl;
  cerr << "  TAM: Tamaño del vector (>0)" << endl;
  cerr << "  CASO: Caso mejor, peor o promedio. Leyenda:" << endl;
  cerr << "    - Caso mejor: " << CASO_MEJOR << endl;
  cerr << "    - Caso peor: " << CASO_PEOR << endl;
  cerr << "    - Caso promedio: " << CASO_PROMEDIO << endl;
  cerr << "  ALGORITMO: Algoritmo de ordenación deseado. Posibilidades:" << endl;
  cerr << "    - burbuja" << endl;
  cerr << "    - insercion" << endl;
  cerr << "    - seleccion" << endl;
  cerr << "    - abb" << endl;
  cerr << "    - apo" << endl;
  cerr << "    - fuerzabruta" << endl;
  cerr << "    - mergesort" << endl;
  exit(EXIT_FAILURE);
}

int main(int argc, char * argv[])
{
  // Lectura de parámetros
  if (argc != 4)
  sintaxis();
  int tam = atoi(argv[1]);     // Tamaño del vector
  int caso = atoi(argv[2]);    // Caso de ejecución
  string algoritmo = argv[3];  // Algoritmo de ordenación
  if (tam <= 0 ||
    (caso != CASO_MEJOR && caso != CASO_PEOR && caso != CASO_PROMEDIO))
    sintaxis();

  // Generación del vector
  int *v=generaVector(tam, caso, algoritmo);

  // Medición de tiempo
  high_resolution_clock::time_point start, end;
  duration<double> tiempo_transcurrido;
  start = high_resolution_clock::now();

  // Ordenación
  ordenar(v,tam,algoritmo);

  // Generación de resultados
  end = high_resolution_clock::now();
  tiempo_transcurrido  = duration_cast<duration<double> >(end - start);
  cout << tam << "\t" <<tiempo_transcurrido.count() << endl;

  delete [] v;     // Liberamos memoria dinámica
  }

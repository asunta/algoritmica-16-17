#include "algoritmos.h"

using namespace std;

/** FUNCIONES PRIVADAS **/

int* vectorAleatorio(int tam){
  int* generado = new int[tam];
  for (int i=0; i<tam; i++)
    generado[i] = i;

  random_shuffle(&generado[0], &generado[tam-1]);
  return generado;
}

void swap(int *xp, int *yp)
{
  int temp = *xp;
  *xp = *yp;
  *yp = temp;
}

/**
* @brief Peor caso Mergesort
* Código obtenido de https://stackoverflow.com/questions/24594112/when-will-the-worst-case-of-merge-sort-occur
**/
void unir(int* arr, int* left, int* right, int n_left, int n_right) {
    int i,j;
    for(i=0;i<n_left;i++)
            arr[i]=left[i];
    for(j=0;j<n_right;j++,i++)
            arr[i]=right[j];
}

void separar(int* arr, int tam) {
  if(tam<=1)
      return;

  if(tam==2)
  {
    swap(&arr[0], &arr[1]);
    return;
  }

  int i,j;
  int n_left = (tam + 1) / 2;
  int n_right = tam - n_left;
  int* left = new int[n_left];
  int* right = new int[n_right];

  for(i=0,j=0;i<tam;i=i+2,j++) //Storing alternate elements in left subarray
      left[j]=arr[i];

  for(i=1,j=0;i<tam;i=i+2,j++) //Storing alternate elements in right subarray
      right[j]=arr[i];

  separar(left, n_left);
  separar(right, n_right);
  unir(arr, left, right, n_left, n_right);
}

int* peorMerge(int tam){
  int* arr = new int[tam];
  for(int i=0; i<tam; i++){
    arr[i] = i;
  }
  separar(arr, tam);
  return arr;
}

int* casoMejor(int tam, string algoritmo){
  int* generado = new int[tam];
  if(strcmp(algoritmo.c_str(),"burbuja")==0 ||
    strcmp(algoritmo.c_str(),"insercion")==0 ||
    strcmp(algoritmo.c_str(),"seleccion")==0 ||
    strcmp(algoritmo.c_str(),"mergesort")==0){
      // Vector ordenado
      for(int i=0; i<tam; i++){
        generado[i] = i;
      }
  }
  else if(strcmp(algoritmo.c_str(),"abb")==0 ||
    strcmp(algoritmo.c_str(),"apo")==0){
    // Vector balanceado
    int i=0;
    int power = 2;
    generado[0] = tam/2;
    while(i<tam){
      int j=1;
      while(j < power && i < tam){
          generado[i] = (tam*8/power)*j;
          j=j+2;
          i++;
      }
      power=power*2;
    }
  }
  else if(strcmp(algoritmo.c_str(),"fuerzabruta")==0){
    delete [] generado;
    generado = vectorAleatorio(tam);
  }
  else{
    cerr << "El algoritmo especificado no es válido" << endl;
    delete [] generado;
  }

  return generado;
}

int* casoPeor(int tam, string algoritmo){
  int* generado = new int[tam];
  if(strcmp(algoritmo.c_str(),"burbuja")==0 ||
    strcmp(algoritmo.c_str(),"insercion")==0 ||
    strcmp(algoritmo.c_str(),"seleccion")==0 ){
      // Vector inversamente ordenado
      for(int i=0; i<tam; i++){
        generado[i] = tam-i;
      }
  }
  else if(strcmp(algoritmo.c_str(),"abb")==0 ||
    strcmp(algoritmo.c_str(),"apo")==0){
    // Vector ordenado
    for(int i=0; i<tam; i++){
      generado[i] = i;
    }
  }
  else if(strcmp(algoritmo.c_str(),"fuerzabruta")==0){
    delete [] generado;
    generado = vectorAleatorio(tam);
  }
  else if(strcmp(algoritmo.c_str(),"mergesort")==0){
    generado = peorMerge(tam);
  }
  else{
    cerr << "El algoritmo especificado no es válido" << endl;
    delete [] generado;
  }

  return generado;
}

int* casoPromedio(int tam, string algoritmo){
  return vectorAleatorio(tam);
}

void burbuja(int* v, int n){
  int i, j;
  for (i = 0; i < n-1; i++){
    for (j = 0; j < n-i-1; j++){
      if (v[j] > v[j+1])
        swap(&v[j], &v[j+1]);
    }
  }
}

void insercion(int* v, int n){
  int j;
  for (int i = 0; i < n; i++){
    j = i;
    while (j > 0 && v[j] < v[j-1]){
      swap(&v[j], &v[j-1]);
      j--;
    }
  }
}

void seleccion(int* v, int n){
  int pos_min,temp;

  for (int i=0; i < n-1; i++){
    pos_min = i;

    for (int j=i+1; j < n; j++){

      if (v[j] < v[pos_min])
      pos_min=j;
    }

    if (pos_min != i){
      swap(&v[i], &v[pos_min]);
    }
  }
}

void abb(int* v, int n){
  ABB<int>ab_bus;
  // Inserción en el árbol
  for (int i=0;i<n;i++){
    ab_bus.Insertar(v[i]);
  }

  // Lectura en inorden
  ABB<int>::nodo node;

  int i=0;
  for (node=ab_bus.begin();node!=ab_bus.end();++node){
    v[i]=*node;
    i++;
  }
}

void apo(int* v, int n){
  APO<int>ap_int;

  // Inserción en el árbol
  for(int i=0; i<n; i++){
    ap_int.insertar(v[i]);
  }

  // Extracción de elementos ordenados
  for(int i=0; i<n; i++){
    v[i] = ap_int.minimo();
    ap_int.borrar_minimo();
  }
}

bool vectorOrdenado(vector<unsigned int> pe, int* v){
  for(int i=0; i<pe.size()-1; i++){
    if(v[pe[i+1]-1] < v[pe[i]-1]){
      return false;
    }
  }
  return true;
}


void fuerzabruta(int* v, int n){
  bool ordenado = false;
  Permutacion::iterator it;

  Permutacion per(n,1);
  do{
    it = per.begin();
    ordenado = vectorOrdenado(*it,v);
  }while(!ordenado && per.GeneraSiguiente());

  int* vordenado = new int[n];

  for(int i=0; i<n; i++){
    vordenado[i] = v[(*it)[i] - 1];
  }

  for(int i=0; i<n; i++){
    v[i] = vordenado[i];
  }
}


/** FUNCIONES PÚBLICAS **/

int* generaVector(int tam, int caso, string algoritmo){
  int* generado;

  switch(caso){
    case CASO_MEJOR:
    {
      generado = casoMejor(tam, algoritmo);
    }
    break;

    // Caso peor
    case CASO_PEOR:
    {
      generado = casoPeor(tam, algoritmo);
    }
    break;

    // Caso promedio
    case CASO_PROMEDIO:
    {
      generado = casoPromedio(tam, algoritmo);
    }
    break;

    // Caso no válido
    default:
    {
      cerr << "El valor de caso introducido no es válido" << endl;
    }
    break;
  }

  return generado;
}

void ordenar(int *v, int n, string algoritmo){
  if(strcmp(algoritmo.c_str(),"burbuja")==0){
    burbuja(v,n);
  }
  else if(strcmp(algoritmo.c_str(),"insercion")==0){
    insercion(v,n);
  }
  else if(strcmp(algoritmo.c_str(),"seleccion")==0){
    seleccion(v,n);
  }
  else if(strcmp(algoritmo.c_str(),"abb")==0){
    abb(v,n);
  }
  else if(strcmp(algoritmo.c_str(),"apo")==0){
    apo(v,n);
  }
  else if(strcmp(algoritmo.c_str(),"fuerzabruta")==0){
    fuerzabruta(v,n);
  }
  else if(strcmp(algoritmo.c_str(),"mergesort")==0){
    mergesort(v,n);
  }
  else{
    cerr << "El algoritmo introducido no es válido" << endl;
  }

  for(int i=0; i<n; i++)
    cout << v[i] << " ";
  cout << endl;
}

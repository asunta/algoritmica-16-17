#include "mergesort.h"

using namespace std;

void alg_insercion_lims(int T[], int inicial, int fin)
{
  int i, j;
  int aux;
  for (i = inicial + 1; i < fin; i++) {
    j = i;
    while ((T[j] < T[j-1]) && (j > 0)) {
      aux = T[j];
      T[j] = T[j-1];
      T[j-1] = aux;
      j--;
    };
  };
}

void alg_insercion(int T[], int num_elem)
{
  alg_insercion_lims(T, 0, num_elem);
}


const int UMBRAL_MS = 100;

void mergesort(int* T, int num_elem)
{
  mergesort_lims(T, 0, num_elem);
}

void mergesort_lims(int* T, int inicial, int final)
{
  if (final - inicial < UMBRAL_MS)
    {
      alg_insercion_lims(T, inicial, final);
    } else {
      int k = (final - inicial)/2;

      int * U = new int [k - inicial + 1];
      assert(U);
      int l, l2;
      for (l = 0, l2 = inicial; l < k; l++, l2++)
	U[l] = T[l2];
      U[l] = INT_MAX;

      int * V = new int [final - k + 1];
      assert(V);
      for (l = 0, l2 = k; l < final - k; l++, l2++)
	V[l] = T[l2];
      V[l] = INT_MAX;

      mergesort_lims(U, 0, k);
      mergesort_lims(V, 0, final - k);
      fusion(T, inicial, final, U, V);
      delete [] U;
      delete [] V;
    };
}


void fusion(int* T, int inicial, int final, int* U, int* V)
{
  int j = 0;
  int k = 0;
  for (int i = inicial; i < final; i++)
    {
      if (U[j] < V[k]) {
	T[i] = U[j];
	j++;
      } else{
	T[i] = V[k];
	k++;
      };
    };
}

#!/usr/local/bin/gnuplot

f(x) = a*x + b
fit f(x) ARG1 via a, b
plot ARG1, f(x)

#!/usr/local/bin/gnuplot

f(x) = a*b**x + c
fit f(x) ARG1 via a, b, c
plot ARG1, f(x)

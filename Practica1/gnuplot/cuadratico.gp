#!/usr/local/bin/gnuplot

f(x) = a*x**2 + b*x + c
fit f(x) ARG1 via a, b, c
plot ARG1, f(x)

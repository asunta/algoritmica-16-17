#!/usr/local/bin/gnuplot

f(x) = a*log(b*x + c) + d
fit f(x) ARG1 via a, b, c, d
plot ARG1, f(x)

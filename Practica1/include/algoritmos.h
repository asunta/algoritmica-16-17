#ifndef ALGORITMOS_H
#define ALGORITMOS_H

#include <cstring>
#include <cstdlib>
#include <iostream>
#include "abb.h"
#include "apo.h"
#include "permutacion.h"
#include "mergesort.h"
#include <sstream>
#include <vector>

#define CASO_MEJOR    0
#define CASO_PEOR     1
#define CASO_PROMEDIO 2

int* generaVector(int tam, int caso, string algoritmo);

void ordenar(int *v, int n, string algoritmo);

#endif

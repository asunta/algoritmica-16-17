#ifndef MERGESORT_H
#define MERGESORT_H

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <climits>
#include <cassert>

using namespace std;

/* ************************************************************ */
/*  Método de ordenacin por mezcla  */

/**
   @brief Ordena un vector por el mtodo de mezcla.

   @param T: vector de elementos. Debe tener num_elem elementos.
             Es MODIFICADO.
   @param num_elem: nmero de elementos. num_elem > 0.

   Cambia el orden de los elementos de T de forma que los dispone
   en sentido creciente de menor a mayor.
   Aplica el algoritmo de mezcla.
*/
void mergesort(int* T, int num_elem);



/**
   @brief Ordena parte de un vector por el mtodo de mezcla.

   @param T: vector de elementos. Tiene un nmero de elementos
                   mayor o igual a final. Es MODIFICADO.
   @param inicial: Posicin que marca el incio de la parte del
                   vector a ordenar.
   @param final: Posicin detrs de la ltima de la parte del
                   vector a ordenar.
		   inicial < final.

   Cambia el orden de los elementos de T entre las posiciones
   inicial y final - 1 de forma que los dispone en sentido creciente
   de menor a mayor.
   Aplica el algoritmo de la mezcla.
*/
void mergesort_lims(int* T, int inicial, int final);


/**
   @brief Ordena un vector por el mtodo de insercin.

   @param T: vector de elementos. Debe tener num_elem elementos.
             Es MODIFICADO.
   @param num_elem: nmero de elementos. num_elem > 0.

   Cambia el orden de los elementos de T de forma que los dispone
   en sentido creciente de menor a mayor.
   Aplica el algoritmo de insercin.
*/
void alg_insercion(int* T, int num_elem);


/**
   @brief Ordena parte de un vector por el mtodo de insercin.

   @param T: vector de elementos. Tiene un nmero de elementos
                   mayor o igual a final. Es MODIFICADO.
   @param inicial: Posicin que marca el incio de la parte del
                   vector a ordenar.
   @param final: Posicin detrs de la ltima de la parte del
                   vector a ordenar.
		   inicial < final.

   Cambia el orden de los elementos de T entre las posiciones
   inicial y final - 1 de forma que los dispone en sentido creciente
   de menor a mayor.
   Aplica el algoritmo de la insercin.
*/
void alg_insercion_lims(int* T, int inicial, int final);


/**
   @brief Mezcla dos vectores ordenados sobre otro.

   @param T: vector de elementos. Tiene un nmero de elementos
                   mayor o igual a final. Es MODIFICADO.
   @param inicial: Posicin que marca el incio de la parte del
                   vector a escribir.
   @param final: Posicin detrs de la ltima de la parte del
                   vector a escribir
		   inicial < final.
   @param U: Vector con los elementos ordenados.
   @param V: Vector con los elementos ordenados.
             El nmero de elementos de U y V sumados debe coincidir
             con final - inicial.

   En los elementos de T entre las posiciones inicial y final - 1
   pone ordenados en sentido creciente, de menor a mayor, los
   elementos de los vectores U y V.
*/
void fusion(int* T, int inicial, int final, int U[], int V[]);

#endif

#!/bin/csh
if ($#argv != 2) then
  echo "Sintaxis: $0 <caso> <algoritmo>"
  echo "Casos posibles (número entero)"
  echo "  0. Caso mejor"
  echo "  1. Caso peor"
  echo "  2. Caso promedio"
  echo "Algoritmos:"
  echo "  - burbuja"
  echo "  - insercion"
  echo "  - seleccion"
  echo "  - abb"
  echo "  - apo"
  echo "  - fuerzabruta"
  echo "  - mergesort"
else
  set algoritmo = "$2"
  set fichero = "data/tiempos_${algoritmo}"

  if ($algoritmo != "fuerzabruta") then
    @ inicio = 100
    @ fin = 50000
    @ incremento = 100
  else
    @ inicio = 1
    @ fin = 11
    @ incremento = 1
  endif

  @ i = $inicio
  @ caso = $1


  if ($caso == 0) then
    set fichero = "${fichero}_mejor.dat"
  else if ($caso == 1) then
    set fichero = "${fichero}_peor.dat"
  else
    set fichero = "${fichero}_promedio.dat"
  endif

  echo > $fichero
  while ( $i <= $fin )
    echo Ejecución tam = $i
    echo `./binaries/ordenacion $i $caso $algoritmo` >> $fichero
    @ i += $incremento
  end
endif

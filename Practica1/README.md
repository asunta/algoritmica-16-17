# Práctica 1

## Directorios

* **Binaries** : Ejecutables de la práctica.
* **Code** : Código C++ de la practica.
* **Data** : Ficheros de medición de tiempos para cada algoritmo.
* **GNUPlot** : Scripts para la generación de gráficas con GNU Plot.
* **Material** : Material proporcionado por el equipo docente.
* **Memoria** : Memoria de la práctica en LaTeX.

## Uso de la práctica

- Compilación del código

```bash
make
```

- Medición de tiempos mediante el script `tiempos.csh`

```bash
./tiempos.csh 0 burbuja
```

- Generación de las gráficas mediante el script `ajuste.csh`

```bash
./ajuste.csh fichero.dat 1
```

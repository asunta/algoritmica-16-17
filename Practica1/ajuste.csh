#!/bin/csh
if ($#argv != 2) then
  echo "Sintaxis: $0 <fichero> <tipo de ajuste>"
  echo "El tipo de ajuste se expresa como un número entero:"
  echo "  1. Lineal"
  echo "  2. Cuadrático"
  echo "  3. Logarítmico"
  echo "  4. Exponencial"
else
  if ($2 == 1) then
    gnuplot --persist -c gnuplot/lineal.gp $1
  else if ($2 == 2 ) then
    gnuplot --persist -c gnuplot/cuadratico.gp $1
  else if ($2 == 3) then
    gnuplot --persist -c gnuplot/logaritmico.gp $1
  else if ($2 == 4) then
    gnuplot --persist -c gnuplot/exponencial.gp $1
  else
    echo "Tipo de ajuste no válido"
  endif
endif

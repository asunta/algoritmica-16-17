#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
#include <sstream>
#include <string>
#include <cstdlib>
#include <utility>
#include "Apermutacion.h"

#define GREEDY 0
#define BACKTRACKING 1
#define BRANCH_BOUND 2
#define PODA 3

#define INF -1

using namespace std;

int calculaDiscrepancia(vector<pair<int,int>> parejas, vector<vector<int>> ady){
	int disc = 0;
	for(int i=0; i<parejas.size(); i++){
		disc += ady[parejas[i].first][parejas[i].second];
	}
	return disc;
}

vector<pair<int,int>> getParejas(vector<int> asoc){
	vector<bool> emparejado(asoc.size(), false);
	vector<pair<int,int>> parejas;
	pair<int,int> p;
	for(int i=0; i<asoc.size(); i++){
		if(!emparejado[i]){
			if(asoc[i] == 0){
				emparejado[i] = true;
			}
			else{
				emparejado[i] = true;
				p.first = i;
				emparejado[asoc[i]-1] = true;
				p.second = asoc[i]-1;
				parejas.push_back(p);
			}
		}
	}
	return parejas;
}

int calculaDiscrepancia(vector<int> asoc, vector<vector<int>> ady){
	int disc = 0;
	for(int i=0; i<asoc.size(); i++){
		if(asoc[i] != 0){
			disc += ady[i][asoc[i]-1];
		}
	}
	return disc;
}

int bestMatch(vector<int> fila, vector<bool>& emparejado){
	int best_match, disc_min;
	bool primero = true;
	for(int i=0; i<fila.size(); i++){
		if(!emparejado[i]){
			if(primero){
				best_match = i;
				disc_min = fila[i];
				primero = false;
			}
			else if(fila[i] != -1 && fila[i] < disc_min){
				best_match = i;
				disc_min = fila[i];
			}
		}
	}
	return best_match;
}

int cotaInferior(vector<int> asignaciones, vector<vector<int>> adyacencias){
	int sum = 0;
	int min;
	vector<bool> asignado (asignaciones.size(), false);
	for(int i=0; i<adyacencias.size(); i++){
		if(asignaciones[i] == 0 && !asignado[i]){
			min = adyacencias[i][0];

			for(int j=1; j < adyacencias[i].size(); j++){
				if(adyacencias[i][j] < min && adyacencias[i][j] != INF)
					min = adyacencias[i][j];
			}
			sum += min;
		}
		else if(asignaciones[i] != 0){
			asignado[i] = true;
			asignado[asignaciones[i]-1] = true;
		}
	}
	return sum;
}

int cotaSuperior(vector<int> asignaciones, vector<vector<int>> adyacencias){
	int sum = 0;
	int max;
	vector<bool> asignado (asignaciones.size(), false);
	for(int i=0; i<adyacencias.size(); i++){
		if(asignaciones[i] == 0 && !asignado[i]){
			max = adyacencias[i][0];
			for(int j=1; j < adyacencias[i].size(); j++){
				if(adyacencias[i][j] > max && adyacencias[i][j] != INF)
					max = adyacencias[i][j];
			}
			sum += max;
		}
		else if(asignaciones[i] != 0){
			asignado[i] = true;
			asignado[asignaciones[i]-1] = true;
		}
	}
	return sum;
}

vector<pair<int,int>> greedy(vector<vector<int>> ady){
	vector<pair<int,int>> parejas;
	vector<bool> emparejado;

	for(int i=0; i<ady.size(); i++){
		emparejado.push_back(false);
	}

	pair<int,int> pareja;
	for(int i=0; i<ady.size(); i++){
		if(!emparejado[i]){
			emparejado[i] = true;
			pareja.first = i;
			pareja.second = bestMatch(ady[i], emparejado);
			emparejado[pareja.second] = true;
			parejas.push_back(pareja);
		}
	}

	return parejas;
}

vector<pair<int,int>> backtracking(vector<vector<int>> ady){
	Apermutacion per(ady.size());
	vector<int> best, buf;
	int disc_min, disc_actual;
	bool primera = true;
	int nodos = 0;

	do{
		buf = per.GetSecuencia();
		if(per.GetLevel() == ady.size() - 1){
			disc_actual = calculaDiscrepancia(buf,ady);
			if(primera){
				primera = false;
				best = buf;
				disc_min = disc_actual;
			}
			else if(disc_actual < disc_min){
				best = buf;
				disc_min = disc_actual;
			}
		}
		nodos++;
	}while(per.GeneraSiguienteProfundidad());

	int total = per.NumeroSecuenciasPosibles();
	cout << "Número de nodos recorridos: " << nodos << endl;
	cout << "Total nodos: " << total << endl;
	cout << "Porcentaje: " << (nodos*1.0)/(total*1.0)*100 << endl;

	return getParejas(best);
}

vector<pair<int,int>> poda(vector<vector<int>> ady){
	Apermutacion per(ady.size());
	vector<int> best, buf;
	int disc_min, disc_actual, disc_est; // Discrepancia mínima, actual, estimada
	bool primera = true;
	int nodos = 0;
	int vacio = false;

	do{
		buf = per.GetSecuencia();

		// Calcular discrepancia estimada
		disc_est = calculaDiscrepancia(buf, ady) + cotaInferior(buf, ady);
		while(!primera && disc_est > disc_min && !vacio){
			vacio = !per.Backtracking();
			if(!vacio){
				buf = per.GetSecuencia();
				disc_est = calculaDiscrepancia(buf, ady) + cotaInferior(buf, ady);
			}
		}
		if(!vacio && per.GetLevel() == ady.size() - 1){
			disc_actual = calculaDiscrepancia(buf,ady);
			if(primera){
				primera = false;
				best = buf;
				disc_min = disc_actual;
			}
			else if(disc_actual < disc_min){
				best = buf;
				disc_min = disc_actual;
			}
		}
		nodos++;
		if(!vacio){
			vacio = !(per.GeneraSiguienteProfundidad());
		}
	}while(!vacio);

	int total = per.NumeroSecuenciasPosibles();
	cout << "Número de nodos recorridos: " << nodos << endl;
	cout << "Total nodos: " << total << endl;
	cout << "Porcentaje: " << (nodos*1.0)/(total*1.0) << endl;

	return getParejas(best);
}

bool merecePena(vector<int> sec_nodo, vector<vector<int>> ady, vector<int> secuencia){
	int disc = calculaDiscrepancia(sec_nodo, ady) + cotaInferior(sec_nodo, ady);
	return disc < calculaDiscrepancia(secuencia, ady);
}

float discrepanciaMedia(vector<int> secuencia, vector<vector<int>> ady){
	int cota_i = cotaInferior(secuencia, ady);
	int cota_s = cotaSuperior(secuencia, ady);
	return (cota_i*1.0 + cota_s)/2.0;
}

void swap(vector<vector<int>>& pers, vector<float>& flotantes, int i, int j){
	vector<int> buf_per = pers[i];
	float buf_flo = flotantes[i];

	pers[i] = pers[j];
	pers[j] = buf_per;

	flotantes[i] = flotantes[j];
	flotantes[j] = buf_flo;
}

void ordenarNodos(vector<vector<int>>& nodos, vector<vector<int>> ady){
	vector<float> costes(nodos.size(),0);
	for(int i=0; i<costes.size(); i++){
		costes[i] = discrepanciaMedia(nodos[i], ady);
	}

	for(int i=0; i<nodos.size()-1; i++){
		for(int j=i+1; j<nodos.size(); j++){
			if(costes[j] < costes[i]){
				swap(nodos, costes, i, j);
			}
		}
	}
}

void recurBranch(Apermutacion padre, vector<vector<int>> ady,
								 vector<int>& secuencia, bool& primero){
	vector<int> min_secuencia;
	int disc_min, disc_actual;
	vector<vector<int>> hermanos = padre.GeneraHijos();
	int level = padre.GetLevel() + 1;
	
	if(level == ady.size() - 1){
		primero = false;
		disc_min = calculaDiscrepancia(hermanos[0], ady);
		min_secuencia = hermanos[0];
		for(int i=1; i<hermanos.size(); i++){
			disc_actual = calculaDiscrepancia(hermanos[i], ady);
			if(disc_actual < disc_min){
				disc_min = disc_actual;
				min_secuencia = hermanos[i];
			}
		}
		secuencia = min_secuencia;
	}
	else{
		ordenarNodos(hermanos, ady);
		for(int i=0; i<hermanos.size(); i++){
			if(primero || merecePena(hermanos[i], ady, secuencia)){
				Apermutacion sigPadre(hermanos[i], level);
				recurBranch(sigPadre, ady, secuencia, primero);
			}
		}
	}
}

vector<pair<int,int>> branch(vector<vector<int>> ady){
	vector<int> sec_inicial(ady.size(),0);
	Apermutacion per(sec_inicial,-1);
	bool primero = true;
	vector<int> secuencia;
	recurBranch(per, ady, secuencia, primero);

	int disc = calculaDiscrepancia(secuencia, ady);
	vector<pair<int,int>> parejas = getParejas(secuencia);
	return parejas;
}

int calculaParejas(vector<vector<int>>adyacencias,
									 vector<pair<int,int>>& parejas, int algoritmo){
	switch(algoritmo){
		case GREEDY:
		{
			cout << "Algoritmo greedy" << endl;
			parejas = greedy(adyacencias);
		}
		break;
		case BACKTRACKING:
		{
			cout << "Algoritmo backtracking" << endl;
			parejas = backtracking(adyacencias);
		}
		break;
		case PODA:
		{
			cout << "Algoritmo backtracking con poda" << endl;
			parejas = poda(adyacencias);
		}
		break;
		case BRANCH_BOUND:
		{
			cout << "Algoritmo branch and bound" << endl;
			parejas = branch(adyacencias);
		}
		break;
	}
	return calculaDiscrepancia(parejas, adyacencias);
}

int calculaDiferencia(vector<int> a, vector<int> b){
	int dif = 0;
	for(unsigned int i=0; i<a.size(); i++){
		dif += abs(a[i] - b[i]);
	}
	return dif;
}

vector<vector<int>> calculaAdyacencias(vector<vector<int>> datos){
	vector<vector<int>> ady;
	int diferencia;

	for(unsigned int i=0; i<datos.size(); i++){
		vector<int> fila;
		for(unsigned int j=0; j<datos.size(); j++){
			if(i==j)
				fila.push_back(-1);
			else
				fila.push_back(0);
		}
		ady.push_back(fila);
	}

	for(unsigned int i=0; i<datos.size()-1; i++){
		for(unsigned int j=i; j<datos.size(); j++){
			if(i!=j){
				diferencia = calculaDiferencia(datos[i], datos[j]);
				ady[i][j] = diferencia;
				ady[j][i] = diferencia;
			}
		}
	}

	return ady;
}

vector<vector<int>> leeFichero(string nombre){
  string line;
  ifstream file;
  vector<vector<int>> datos;

  file.open(nombre);

  // Ignoramos la primera línea
  getline(file, line);

	// Leemos las primeras dos palabras
	int personas, preguntas;
	file >> personas;
	file >> preguntas;

	int buffer;
  for(int i=0; i<personas; i++){
    vector<int> fila;
    for(int j=0; j<preguntas; j++){
			file >> buffer;
      fila.push_back(buffer);
    }
    datos.push_back(fila);
  }

  file.close();

  return datos;
}

void sintaxis(){
  cerr << "Sintaxis: FRIENDSHIP <fichero> <algoritmo>" << endl;
  cerr << "  Algoritmos:" << endl;
  cerr << "  - greedy" << endl;
  cerr << "  - backtracking" << endl;
	cerr << "  - poda" << endl;
  cerr << "  - branch" << endl;
}

int stringToAlg(string nombre){
  if(strcmp(nombre.c_str(), "greedy") == 0){
    return GREEDY;
  }
  else if(strcmp(nombre.c_str(), "backtracking") == 0){
    return BACKTRACKING;
  }
  else if(strcmp(nombre.c_str(), "branch") == 0){
    return BRANCH_BOUND;
  }
	else if(strcmp(nombre.c_str(), "poda") == 0){
    return PODA;
  }
  else{
    return -1;
  }
}

int main(int argc, char* argv[]){

  if(argc != 3){
    sintaxis();
    exit(0);
  }

  string fichero = argv[1];
  string alg_str = argv[2];
  int algoritmo = stringToAlg(alg_str);
  if(algoritmo == -1){
    cerr << "El algoritmo escogido no es correcto" << endl;
    sintaxis();
    exit(-1);
  }

  vector<vector<int>> datos = leeFichero(fichero);
	vector<vector<int>> adyacencias = calculaAdyacencias(datos);

	vector<pair<int,int>> parejas;
	int discrepancia = calculaParejas(adyacencias, parejas, algoritmo);

	for(int i=0; i<parejas.size(); i++){
		cout << "Persona " << parejas[i].first + 1 << " asignamos a " << parejas[i].second + 1 << endl;
	}
	cout << "Discrepancia: " << discrepancia << endl;
}
